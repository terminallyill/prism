![Prism Frontend Example](https://gitlab.com/terminallyill/prism/-/raw/master/readme_images/prism-screenshot.png?ref_type=heads)

## Prism
Prism is a graphical network analyzer and man-in-the-middle platform, written in Rust with a retro aesthetic.

Think Postman meets mitmproxy with the ability to automatically generate Swagger pages for select requests.

This is **experimental** and can be considered a proof of concept for now.

### Architecture
Below is a general architecture diagram for the project.

Prism currently relies on a mitmproxy server and an accompanying script to relay intercepted traffic to the backend. However, it is a project goal to become a single application, similar to Charles.

![Prism Architecture](https://gitlab.com/terminallyill/prism/-/raw/master/readme_images/prism.architecture.png?ref_type=heads)

The frontend is powered by [Dioxus](https://github.com/DioxusLabs/dioxus).
The backend is powered by [Actix-Web](https://github.com/actix/actix-web).

### Introduction
Prism currently consists of three major pieces:
- A script for mitmproxy, which relayes intercepted requests to a backend
- A backend, built on actix-web, that both manages the request relays and serves the captured requests to the frontend
- A frontend, built in dioxus-web, to visually navigate received requests, allow for replays / modifications to new / captured requests

### Compatibility

This project should run on any operating system or architecture (x86/x64/ARM/etc) with python and rust installed.

### Getting Started

You will need to run three separate tabs:
- 1x to run the mitmproxy script
- 1x to run the backend server
- 1x to run the frontend UI

Assuming both Python and Rust are installed, first, clone the project:
```git clone https://gitlab.com/terminalillness/prism```

#### Starting mitmproxy
Next, either move the script the folder containing mitmproxy, or, move mitmproxy into the scripts/ folder of this project and run:
```./mitmproxy --script prism-capture.py```

#### Starting the backend
In the second tab, navigate to the backend directory and run:
```cargo run```

This will download / install all dependencies for the project and launch the backend

#### Starting the frontend
In the third tab, navigate to the frontend directory and run:
```dx serve --port 8000```

You can now access the frontend of the project by visiting localhost:8069

### Accessing Swagger Docs
Once requests are either captured or manually requested, you can save them within the UI / frontend.

These will show up under the SAVED tab.

You can export these requests, which will send a request to the backend and update the OpenAPI formatted file that is connected to the endpoing providing the docs.

http://localhost:8069

### Changelog
v1.0.0
Initial Commit
Features:
- Make manual requests, view responses
- Request history
- Save requests, generate Swagger pages
Notes:
- Barebones UI framework
- Desktop only
