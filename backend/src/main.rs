use actix_web::{
    get, 
    post, 
    web, 
    route,
    App,
    HttpResponse, 
    HttpRequest,
    HttpServer, 
    Responder,
    Result,
    Error
};

use actix_cors::Cors;
use actix_web_actors::ws;
use actix_files::{Files, NamedFile};
use actix::{Actor, Context, StreamHandler};
use awc::Client;
use awc::http::header;

use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_json::to_string;
use serde_json::Value;
use std::sync::Mutex;

#[derive(Deserialize, Serialize, Clone)]
struct CapturedRequest {
    name: String,
    method: String,
    url: String,
    headers: HashMap<String, String>
}

#[derive(Deserialize, Serialize, Clone)]
struct SimpleCapture {
    name: String,
    url: String,
    method: String,
    headers: HashMap<String, String>,
}

#[post("/echo")]
async fn echo(req_body: String) -> impl Responder {
    HttpResponse::Ok().body(req_body)
}

async fn manual_hello() -> impl Responder {
    HttpResponse::Ok().body("Hey there!")
}

#[derive(Deserialize, Serialize, Clone)]
struct ReceivedRequest {
    name: String,
    method: String,
    url: String,
    headers: HashMap<String, String>
}

async fn catch_all(
    req: HttpRequest,
    payload: web::Json<ReceivedRequest>,
    data: web::Data<Mutex<Vec<CapturedRequest>>>,
) -> Result<impl Responder> {
    let headers_map = req.headers().iter()
        .map(|(key, value)| {
            (key.to_string(), value.to_str().unwrap_or_default().to_string())
        })
        .collect::<HashMap<_, _>>();
    // You can modify here to capture more details as per your CapturedRequest struct
    let captured_request = CapturedRequest {
        name: format!("{:?}", req.method()),
        method: format!("{:?}", req.method()),
        url: payload.url.clone(),
        headers: headers_map
    };
    
    // Add captured request to the list
    data.lock().unwrap().push(captured_request);

    Ok(HttpResponse::Ok().finish())
}

#[get("/captured")]
async fn captured(data: web::Data<Mutex<Vec<CapturedRequest>>>) -> Result<impl Responder> {
    Ok(web::Json(data))
}

#[get("/capture_2")]
async fn capture2(data: web::Data<Mutex<Vec<SimpleCapture>>>) -> Result<impl Responder> {
    let captured_data = data.lock().unwrap();
    Ok(web::Json(captured_data.clone()))
}

#[derive(Serialize, Clone)]
struct ForwardedResponse {
    body: String,
    headers: HashMap<String, String>
}

#[derive(Serialize)]
struct RessyCapture {
    response: ForwardedResponse,
    status: u16,
}

#[post("/add_captured")]
async fn add_captured(data: web::Data<Mutex<Vec<SimpleCapture>>>, capture: web::Json<SimpleCapture>) -> web::Json<RessyCapture> {
    let req_url = &capture.url;
    let req_name = &capture.name;
    let req_method = &capture.method.to_string();
    let req_headers = &capture.headers;
    data.lock().unwrap().push(SimpleCapture{name: req_name.to_string(), url: req_url.to_string(), method: req_method.to_string(), headers: req_headers.clone()});
    //let mut resp = Client::new()
        //.get(req_url)
        //.headers(req_headers)
        //.send()
        //.await
        //.unwrap();

    let client = Client::new();
    let mut builder = match req_method.as_str() {
        "GET" => client.get(req_url),
        "POST" => client.post(req_url),
        _ => {
            println!("Error - unknown request type!");
            // Default to get request
            client.get(req_url)
        }
    };

    for (key, value) in req_headers.iter() {
        let head_name = header::HeaderName::from_lowercase(key.as_bytes()).unwrap();
        let head_value = header::HeaderValue::from_str(value).unwrap();
        builder = builder.insert_header((head_name, head_value));
    }

    let mut resp = builder.send().await.unwrap();

    let resp_headers = resp.headers()
        .iter()
        .map(|(key, value)| {
            (key.to_string(), value.to_str().unwrap_or("").to_string())
        })
        .collect();
    
    let resp_status = resp.status().as_u16();
    
    let resp_bytes = resp.body().await.unwrap();
    let resp_text = String::from_utf8_lossy(&resp_bytes).to_string();
    
    //let resp_text = String::from_utf8_lossy(&resp).to_string();

    let fwd_resp = ForwardedResponse {
        body: resp_text,
        headers: resp_headers,
    };

    web::Json(RessyCapture {
        response: fwd_resp,
        status: resp_status,
    })        
}


struct MyWebsocket {
    data: web::Data<Mutex<Vec<SimpleCapture>>>,
}

impl MyWebsocket {
    pub fn new(data: web::Data<Mutex<Vec<SimpleCapture>>>) -> Self {
        MyWebsocket {data}
    }
}

impl Actor for MyWebsocket {
    type Context = ws::WebsocketContext<Self>;
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for MyWebsocket {
    fn handle (
        &mut self,
        msg: Result<ws::Message, ws::ProtocolError>,
        ctx: &mut Self::Context,
    ) {
        match msg {
            Ok(ws::Message::Ping(ping)) => {
                ctx.pong(&ping);
            }
            Ok(ws::Message::Text(text)) => {
                let locked_data = self.data.lock().unwrap();
                let response = match to_string(&*locked_data) {
                    Ok(string) => string,
                    Err(e) => {
                        println!("Error serializing data: {}", e);
                        return;
                    }
                };

                ctx.text(response);
            }
            Ok(ws::Message::Binary(bin)) => {
                ctx.binary(bin);
            }
            _ => {}
        }
    }
}

#[get("/captured_output")]
async fn websocket_route(data: web::Data<Mutex<Vec<SimpleCapture>>>, req: HttpRequest, stream: web::Payload) -> Result<HttpResponse, Error> {
    ws::start(MyWebsocket::new(data), &req, stream)
}

#[derive(Clone, Deserialize, Serialize, Debug)]
struct OpenApi {
    openapi: String,
    info: Info,
    paths: HashMap<String, Value>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct Info {
    title: String,
    version: String
}

#[derive(Clone)]
struct CurrentAPI {
    openapi_data: Option<OpenApi>
}

async fn receive_openapi_config(data: web::Json<OpenApi>, state: web::Data<Mutex<CurrentAPI>>) -> impl Responder {
    let mut state_data = state.lock().unwrap();
    state_data.openapi_data = Some(data.into_inner());
    HttpResponse::Ok().body("Received!")
}

async fn serve_openapi_config(state: web::Data<Mutex<CurrentAPI>>) -> impl Responder {
    let state_data = state.lock().unwrap();
    match &state_data.openapi_data {
        Some(data) => HttpResponse::Ok().json(data),
        None => HttpResponse::NotFound().body("no config has been set!")
    }
}


// Update this to use a better 
// Struct for manual requests
// Expected result is to have
// A clean disect that can be ingested
// By the frontend

#[actix_web::main]
async fn main() -> std::io::Result<()> {

    let app_name_mutex = web::Data::new(Mutex::new(String::from("My fave app")));
    let captured_requests_mutex : web::Data<Mutex<Vec<SimpleCapture>>> = web::Data::new(Mutex::new(Vec::new()));
    let final_captures_mutex : web::Data<Mutex<Vec<CapturedRequest>>> = web::Data::new(Mutex::new(Vec::new()));
    
    let starter_config : web::Data<Mutex<CurrentAPI>> = web::Data::new(Mutex::new(CurrentAPI { openapi_data: None}));

    let file = NamedFile::open("static/get-boinger.json");

    HttpServer::new(move || {
        let cors = Cors::permissive();
        App::new()
            .wrap(cors)
            .app_data(app_name_mutex.clone())
            .app_data(captured_requests_mutex.clone())
            .app_data(final_captures_mutex.clone())
            .app_data(starter_config.clone())
            .service(captured)
            .service(capture2)
            .service(add_captured)
            .service(websocket_route)
            .route("/receive_config", web::post().to(receive_openapi_config))
            .route("/serve_config", web::get().to(serve_openapi_config))
            .service(Files::new("/", "./static/").index_file("index.html"))
            .route("/hey", web::get().to(manual_hello))
            .route("/{tail:.*}", web::route().to(catch_all))
    })
    .bind(("127.0.0.1", 8069))?
    .run()
    .await
}