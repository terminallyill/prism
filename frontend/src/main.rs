#![allow(non_snake_case)]

use std::fmt;
use std::collections::HashMap;
use std::str::FromStr;
use serde::{Deserialize, Serialize};
use serde_json::json;
use serde_json::Value;

use dioxus::prelude::*;
use dioxus_web::Config;
use dioxus_router::prelude::*;
use chrono::prelude::*;

#[derive(Routable, Clone, PartialEq)]
enum Route {
    #[route("/")]
    App {}
}

fn main() {
    dioxus_web::launch(|cx| render!(Router::<Route> {}));
}

enum TrafficSection {
    Capture,
    Saved,
    History,
}

#[derive(Clone, Debug, PartialEq)]
enum RequestState {
    Loading,
    Completed,
}

#[derive(Clone, Debug)]
struct RequestHistory {
    method: String,
    url: String,
    headers: HashMap<String, String>,
    body: String,
    response: RequestResponse,
    status: RequestState,
}

#[derive(PartialEq, Clone, Debug)]
struct RequestResponse {
    body: String,
    headers: HashMap<String, String>,
    status_code: String,
}

#[derive(Clone, PartialEq)]
struct CurrentRequest {
    name: String,
    description: String,
    url: String,
    method: String,
    parameters: HashMap<String, String>,
    headers: HashMap<String, String>,
    body: String,
    response: Option<RequestResponse>
}

impl Default for CurrentRequest {
    fn default() -> Self {
        Self {
            name: String::new(),
            description: String::new(),
            url: String::new(),
            method: "GET".to_string(),
            parameters: HashMap::new(),
            headers: HashMap::new(),
            body: String::new(),
            response: None
        }
    }
}

type TrafficSaved = Vec<CurrentRequest>;
type TrafficHistory = Vec<NewRequest>;


struct StateRequests {
    history: TrafficHistory,
    saved: TrafficSaved,
    current: CurrentRequest,
    project: ExportInfo
}

#[derive(PartialEq, Clone, Debug)]
struct NewRequest {
   id: usize,
   url: String,
   method: String,
   headers: Option<HashMap<String, String>>,
   parameters: Option<HashMap<String, String>>,
   body: Option<String>,
   response: Option<RequestResponse>,
   status: RequestState
}

struct ResponseData {
    headers: HashMap<String, String>,
    body: String,
    status_code: String,
}

#[derive(Clone, Serialize, Deserialize)]
struct SimpleRequest {
    name: String,
    url: String,
    id: usize,
    method: String,
    headers: HashMap<String, String>,
}

#[derive(Clone, Deserialize)]
struct SimpleResponse {
    response: String,
    status: u16
}
fn make_request(request_data: NewRequest, cx: Scope) {

    let cap_request = use_future(cx, (&request_data,), |(data,)| async move {
        let client = reqwest::Client::builder()
        .build()
        .unwrap();
        let url = "http://localhost:8069/add_captured".to_string();
        let mut request_builder = client.post(url);

        let payload = SimpleRequest{
            id: data.id.clone(),
            url: data.url.clone(),
            method: data.method.clone(),
            name: "Request_description".to_string(),
            headers: data.headers.unwrap(),
        };

        request_builder = request_builder.json(&payload);
        
        let resp = request_builder.send().await.unwrap();

        // Status code of request towards middleware
        let status_code = resp.status().as_u16();

        let resp_json: Value = resp.json().await.unwrap();

        let body = resp_json["response"].clone();
        let status = resp_json["status"].clone();

        let mut resp_headers: HashMap<String, String> = HashMap::new();
        if let Some(obj) = resp_json["response"]["headers"].as_object() {
            resp_headers = obj.iter()
                .map(|(key, value)| (key.clone(), value.as_str().unwrap_or("").to_string()))
                .collect();
        }

        ResponseData {
            headers: resp_headers,
            body: body.to_string(),
            status_code: status.to_string(),
        }

    });

    match cap_request.value() {
        Some(response) => {
            //let resp_data = response_data_ref.clone();
            let mut state = use_shared_state::<StateRequests>(cx).unwrap().write();
            if response.headers.contains_key("content-type") {
                match serde_json::Value::from_str(&response.body) {
                    Ok(refreshed) => {
                        
                        let body_str = refreshed["body"].as_str().unwrap();
                        // Below includes the digest from the forwarded response
                        let pretty_body: Value = serde_json::from_str(&body_str).unwrap();
                        let pretty = serde_json::to_string_pretty(&pretty_body).unwrap();
                        state.current.response = Some(RequestResponse {
                            body: pretty.clone(),
                            headers: response.headers.clone(),
                            status_code: response.status_code.clone().to_string()
                        });

                        if request_data.id < state.history.len() {
                            let history_item = &mut state.history[request_data.id];
                            history_item.status = RequestState::Completed;
                            history_item.response = Some(RequestResponse {
                                body: pretty.clone(),
                                headers: response.headers.clone(),
                                status_code: response.status_code.clone().to_string(),
                            });
                        }
                    }
                    Err(e) => {
                        state.current.body = format!("Error parsing Json: {}", e);
                    }
                }
            } else {
                // Add in other parsing options
                state.current.response = Some(RequestResponse {
                    body: response.body.clone(),
                    headers: response.headers.clone(),
                    status_code: response.status_code.clone().to_string()
                });

                if request_data.id < state.history.len() {
                    let history_item = &mut state.history[request_data.id];
                    history_item.status = RequestState::Completed;
                    history_item.response = Some(RequestResponse {
                        body: response.body.clone(),
                        headers: response.headers.clone(),
                        status_code: response.status_code.clone().to_string(),
                    });
                }
            }
        },
        None => {
           println!("waiting..");
        },
    }
}

fn App(cx: Scope) -> Element {

    // Shared States
    use_shared_state_provider(cx, || StateRequests {
        saved: TrafficSaved::new(),
        history: TrafficHistory::new(),
        current: CurrentRequest::default(),
        project: ExportInfo::default()
    });

    let show_response = use_state(cx, || true);

    // Response Options
    let mut response_body = "response-header".to_string();
    let mut response_headers = "response-header".to_string();
    let mut response_cookies = "response-header".to_string();

    let response_screen = use_state(cx, || "Body");

    // Traffic
    let mut captures_class = "traffic-option".to_string();
    let mut saved_class = "traffic-option".to_string();
    let mut history_class = "traffic-option".to_string();

    let traffic_screen = use_state(cx, || "Captures");

    // Project Settings
    let mut sessions_class = "traffic-option".to_string();
    let mut config_class = "traffic-option".to_string();
    let mut filters_class = "traffic-option".to_string();

    let project_screen = use_state(cx, || "Config");

    let tester = cx.clone();

    cx.render(rsx!{
        div { 
            margin_bottom: "1em",
            background_color: "#505050",
            padding : ".5em",
            span { 
                class: "old-skool-title",
                "> Prism" 
            }
            div {
                float: "right",
                display: "flex",
                "[GITLAB]"
            }
        }
        div { 
            display: "grid",
            grid_template_columns: "45% 45%",
            justify_content: "space-around",
            div {
                class: "request-container",
                div {
                    class: "new-request-container",
                    RequestDisect {}
                    button {
                        class: "response-show old-skool-mini",
                        onclick: move |event| {
                            show_response.set(!show_response);
                        },
                        match show_response.get() {
                            true => {"Hide Response"}
                            _ => {"Show Response"}
                        }
                    }

                    div { class: "response-section",
                        match show_response.get() {
                            true => {rsx!( 
                                div { 
                                    display: "grid", 
                                    grid_template_columns: "30% 30% 30%", 
                                    justify_content: "space-evenly",
                                    margin_bottom: ".5em",
                                    match &*response_screen.get().to_string() {
                                        "Body" => {
                                            response_body = "response-header-selected".to_string();
                                        },
                                        "Headers" => {
                                            response_headers = "response-header-selected".to_string();
                                        },
                                        "Cookies" => {
                                            response_cookies = "response-header-selected".to_string();
                                        },
                                        _ => {
                                        }
                                    }
                                    button {
                                        class: "{&*response_body} old-skool-mini",
                                        onclick: move |event| {
                                            response_screen.set("Body");
                                        },
                                        "Body"
                                    }
                                    button {
                                        class: "{&*response_headers} old-skool-mini",
                                        onclick: move |event| {
                                            response_screen.set("Headers");
                                        },
                                        "Headers"
                                    }
                                    button {
                                        class: "{&*response_cookies} old-skool-mini",
                                        onclick: move |event| {
                                            response_screen.set("Cookies");
                                        },
                                        "Cookies"
                                    }
                                }
                                ResponseDisect {rcontainer: response_screen.get()}
                            ) }
                            _ => {rsx!({})}
                        }
                    }
                }
            }
            div { class: "traffic-container",
            div { class: "traffic-container-content",
                div { 
                    display: "grid", 
                    grid_template_columns: "30% 30% 30%", 
                    justify_content: "space-evenly", 
                    padding: ".5em",
                    match &*traffic_screen.get().to_string() {
                        "Captures" => {
                            captures_class = "traffic-option-selected".to_string();
                        },
                        "Saved" => {
                            saved_class = "traffic-option-selected".to_string();
                        },
                        "History" => {
                            history_class = "traffic-option-selected".to_string();
                        },
                        _ => {
                        }
                    }
                    button {
                        class: "{&*captures_class}",
                        onclick: move |event| {
                            traffic_screen.set("Captures");
                        },
                        "Captures"
                    }
                    button {
                        class: "{&*saved_class}",
                        onclick: move |event| {
                            traffic_screen.set("Saved");
                        },
                        "Saved"
                    }
                    button {
                        class: "{&*history_class}",
                        onclick: move |event| {
                            traffic_screen.set("History");
                        },
                        "History"
                    }
                }
                TrafficView { tcontainer: traffic_screen.get() }
                
            }
            div { 
                margin_top: "1em",
                class: "traffic-settings",
                div {
                    div {
                        class: "export-settings",
                        button {
                            class: "request-submit old-skool-mini",
                            onclick: move |_| {
                                export_saved(cx.clone());
                            },
                            "Export Saved"
                        }
                        button {
                            class: "request-submit old-skool-mini",
                            onclick: move |_| {
                                export_saved(cx.clone());
                            },
                            "Generate Docs"
                        }
                    }
                    div { 
                        class: "traffic-settings-container",
                        div { 
                            class: "traffic-settings-content",
                            margin_top: ".5em",
                            background_color: "#505050",
                            div { 
                                display: "grid", 
                                grid_template_columns: "30% 30% 30%", 
                                justify_content: "space-evenly", 
                                padding: ".5em",
                                match &*project_screen.get().to_string() {
                                    "Sessions" => {
                                        sessions_class = "traffic-option-selected".to_string();
                                    },
                                    "Config" => {
                                        config_class = "traffic-option-selected".to_string();
                                    },
                                    _ => {
                                    }
                                }
                                button {
                                    class: "{&*config_class}",
                                    onclick: move |event| {
                                        project_screen.set("Config");
                                    },
                                    "Config"
                                }
                                button {
                                    class: "{&*sessions_class}",
                                    onclick: move |event| {
                                        project_screen.set("Sessions");
                                    },
                                    "Sessions"
                                }
                                button {
                                    class: "{&*filters_class}",
                                    onclick: move |event| {
                                        project_screen.set("Filters");
                                    },
                                    "Filters"
                                }
                            }
                            ProjectScreen { container: project_screen.get() }
                            
                        }    
                    }
                }
            }
        }
        }
    })
}

fn export_saved(cx: Scope) {
    let state = use_shared_state::<StateRequests>(cx).unwrap().write();
    let exportable = state.saved.clone();
    let project = state.project.clone();

    let send_future = use_future(cx, (&exportable, &project), |(exportable, project)| async move {

        let mut paths = HashMap::new();

        for request in exportable {
            let method = request.method.to_lowercase();
            let parameters: Vec<Value> = request.parameters.iter()
                .map(|(name, _)| {
                    json!({
                        "name": name,
                        "in": "query",
                        "required": true,
                        "schema": {"type": "string"}
                    })
                })
                .collect();
            
            let headers: Vec<Value> = request.headers.iter()
                .map(|(name, _)| {
                    json!({
                        "name": name,
                        "in": "header",
                        "required": true,
                        "schema": {"type": "string"}
                    })
                })
                .collect();

            let path_item = json!({
                method: {
                    "summary": request.name,
                    "description": request.description,
                    "parameters": parameters.into_iter().chain(headers.into_iter()).collect::<Vec<_>>(),
                    "requestBody": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "example": request.body
                                }
                            }
                        }
                    },
                    // Include expected responses later
                }
            });

            paths.insert(request.url, path_item);
        }

        let openapi_config = json!({
            "openapi": "3.0.0",
            "info": {
                "title": project.title,
                "version": project.version
            },
            "paths": paths
        });

        let client = reqwest::Client::builder().build()?;
        let url = "http://localhost:8069/receive_config".to_string();

        let resp = client.post(url)
            .json(&openapi_config)
            .send()
            .await;

        resp
    });

    match send_future.value() {
        Some(Ok(resp)) => {
            if resp.status().is_success() {
                println!("Created the export request!!");
            } else {
                println!("Failed creating the config..");
            }
        },
        Some(Err(e)) => {
            println!("error when sending the config");
        },
        None => {
            println!("asdfasdfadsf")
        },
    }
}

struct OpenAPIConfig {
    openapi: String,
    info: ExportInfo,
    paths: HashMap<String, Value>
}

#[derive(Clone, PartialEq)]
struct ExportInfo {
    title: String,
    version: String
}

impl Default for ExportInfo {
    fn default() -> Self {
        Self {
            title: String::new(),
            version: String::new(),
        }
    }
}

#[derive(Props)]
struct RequestOptions<'a> {
    roption: &'a str,
}

fn update_request_field(input: &str, value: String, cx: Scope) {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().write();
    match input {
        "title" => {
            state.project.title = value;
        }
        "version" => {
            state.project.version = value;
        }
        "name" => {
            state.current.name = value;
        }
        "description" => {
            state.current.description = value;
        }
        "url" => {
            state.current.url = value;
        }
        "body" => {
            state.current.body = value;
        }
        "method" => {
            state.current.method = value;
        }
        "content-type" => {
            let content_header = format!("{}", &"content-type");
            let content_value = format!("{}", &value);

            if state.current.headers.contains_key("content-type") {
                state.current.headers.remove("content-type");
                if value != "none" {
                    state.current.headers.insert(content_header, content_value);
                }
            } else {
                if value != "none" {
                    state.current.headers.insert(content_header, content_value);
                }
            }
        }
        _ => {}
    }
}

fn validate_request(
    rurl_clone: String,
    rmethod: String,
    headermap: &HashMap<String, String>,
    paramparty: &HashMap<String, String>,
    cx: Scope
) -> Result<NewRequest, &'static str> {
   
    if rurl_clone.trim().is_empty() {
        return Err ("Url is empty");
    }

    if reqwest::Url::parse(&rurl_clone).is_err() {
        return Err("Unable to parse URL");
    }

    let mut state = use_shared_state::<StateRequests>(cx).unwrap().write();

    let request_id = state.history.len();

    let new_request = NewRequest {
        id: request_id,
        url: state.current.url.clone(),
        method: state.current.method.clone(),
        headers: Some(headermap.clone()),
        parameters: Some(paramparty.clone()),
        body: None,
        response: None,
        status: RequestState::Loading,
    };

    state.history.push(new_request.clone());

    Ok(new_request)
}

fn RequestDisect(cx: Scope) -> Element {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().read();
    
    // Current request state clones
    let req_name = state.current.name.clone();
    let req_description = state.current.description.clone();
    
    let rmethod = state.current.method.clone();
    let rurl = state.current.url.clone();

    let req_parameters = state.current.parameters.clone();
    let req_body = state.current.body.clone();
    let req_headers = state.current.headers.clone();

    let rlast_updated = chrono::Utc::now();

    let rurl_clone = state.current.url.clone();

    let show_request_info = use_state(cx, || false); 

    let request_screen = use_state(cx, || "parameters");

    // Request Options
    let mut request_parameters = "response-header".to_string();
    let mut request_body = "response-header".to_string();
    let mut request_headers = "response-header".to_string();

    let mut paramparty = HashMap::new();
    for (key, value) in &state.current.parameters {
        paramparty.insert(key.to_string(), value.to_string());
    }
    
    let mut headermap = HashMap::new();
    for (key, value) in &state.current.headers {
        headermap.insert(key.to_string(), value.to_string());
    }

    cx.render(rsx! {
        div {
            display: "flex",
            margin_bottom: ".5em",
            justify_content:  "space-evenly",
            div { 
                display: "grid",
                grid_template_columns: "30% 30% 30%",
                justify_content: "space-evenly",
                width: "100%",
                button {
                    class: "request-submit old-skool-mini",
                    onclick: move |_| {
                        match validate_request(
                            rurl_clone.clone(),
                            rmethod.clone(),
                            &headermap,
                            &paramparty,
                            cx.clone()
                        ) {
                            Ok(new_request) => {
                                make_request(new_request, cx.clone());
                            }
                            Err(error_message) => {
                                println!("Error!");
                            }
                        }
                    },
                    "Submit"
                }
                button {
                    class: "request-save old-skool-mini",
                    onclick: move |_| {
                        add_saved(cx.clone())
                    },
                    "Save"
                }
                button {
                    class: "request-info old-skool-mini",
                    background_color: "#F4E409",
                    color: "#000000",
                    onclick: move |event| {
                        show_request_info.set(!show_request_info);
                    },
                    match show_request_info.get() {
                        true => {"Hide Details"}
                        _ => {"Show Details"}
                    }
                }
            }
            
        }
        div {
            class: "request-description-container",
            match show_request_info.get() {
                true => {rsx!(
                    input {
                        class: "request-name old-skool-mini",
                        width: "100%",
                        placeholder: "Name of request",
                        oninput: move |evt| {
                            update_request_field("name", evt.value.clone(), cx.clone())
                        },
                        "{req_name}"
                    }
                    textarea {
                        class: "request-description old-skool-mini",
                        placeholder: "Request description",
                        oninput: move |evt| {
                            update_request_field("description", evt.value.clone(), cx.clone())
                        },
                        "{req_description}"
                    }
                )}
                _ => {rsx!({})}
            }
        }
        div {
            div { margin_bottom: ".5em", display: "flex", width: "100%",
                div { display: "flex", width: "100%",
                    select {
                        class: "request-type old-skool-mini",
                        onchange: move |evt| {
                            update_request_field("method", evt.value.clone(), cx.clone())
                        },
                        option { value: "GET", "GET" }
                        option { value: "POST", "POST" }
                        option { value: "DELETE", "DELETE" }
                        option { value: "PUT", "PUT" }
                    }
                    input {
                        class: "request-input old-skool-mini",
                        value: "{rurl}",
                        placeholder: "Enter a url",
                        oninput: move |evt| {
                            update_request_field("url", evt.value.clone(), cx.clone())
                        }
                    }
                }
            }
            div { 
                display: "grid", 
                grid_template_columns: "30% 30% 30%", 
                justify_content: "space-evenly",
                margin_bottom: ".5em",
                match &*request_screen.get().to_string() {
                    "parameters" => {
                        request_parameters = "response-header-selected".to_string();
                    },
                    "body" => {
                        request_body = "response-header-selected".to_string();
                    },
                    "headers" => {
                        request_headers = "response-header-selected".to_string();
                    },
                    _ => {
                    }
                }
                button {
                    class: "{&*request_parameters} old-skool-mini",
                    onclick: move |event| {
                        request_screen.set("parameters");
                    },
                    "Parameters"
                }
                button {
                    class: "{&*request_body} old-skool-mini",
                    onclick: move |event| {
                        request_screen.set("body");
                    },
                    "Body"
                }
                button {
                    class: "{&*request_headers} old-skool-mini",
                    onclick: move |event| {
                        request_screen.set("headers");
                    },
                    "Headers"
                }
            }
            div {
                match &*request_screen.get().to_string() {
                    "parameters" => {rsx!( ParamOptions {}) } 
                    "body" => {rsx!( BodyOptions {})}
                    "headers" => {rsx!( HeaderOptions {} )}
                    _ => {rsx!(CaptureView{})}
                }
            }

        }
    })
}

fn add_parameter(key: String, value: String, cx: Scope) {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().write();
    state.current.parameters.insert(key, value);
}

fn remove_parameter(key: &str, cx: Scope) -> Option<String> {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().write();
    state.current.parameters.remove(key)
}

fn ParamOptions(cx: Scope) -> Element {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().read();

    let cloned_parameters = state.current.parameters.clone();
    
    let new_parameter_name = use_state(cx, || String::new());
    let new_parameter_value = use_state(cx, || String::new());

    cx.render(rsx!{
        div { class: "params",
            div { class: "request-option-container",
                border: ".25em inset black",
                div {
                    padding: ".25em",
                    background_color: "lime",
                    color: "black",
                    justify_content: "space-between",
                    display: "flex",
                    span {
                        "key"
                    }
                    span {
                        "value"
                    }
                    span {
                        ""
                    }
                }
                table { 
                    width: "100%", 
                    border_collapse: "collapse",
                    for (key , value) in cloned_parameters {
                        tr {
                            td { class: "option-container", input { class: "option-input old-skool-mini", value: "{key}" } }
                            td { class: "option-container", input { class: "option-input old-skool-mini", value: "{value}" } }
                            td { class: "option-container",
                                button {
                                    class: "option-input",
                                    onclick: move |_| {
                                        let key_clone = key.clone();
                                        remove_parameter(&key_clone, cx.clone());
                                    },
                                    "x"
                                }
                            }
                        }
                    }
                    tr {
                        td { class: "option-container",
                            input {
                                class: "option-input",
                                placeholder: "Param Option 1",
                                oninput: move |evt| new_parameter_name.set(evt.value.clone()),
                                "{new_parameter_name}"
                            }
                        }
                        td { class: "option-container",
                            input {
                                class: "option-input",
                                placeholder: "Value 1",
                                oninput: move |evt| new_parameter_value.set(evt.value.clone()),
                                "{new_parameter_value}"
                            }
                        }
                        td { class: "option-container",
                            button {
                                class: "option-input",
                                onclick: move |_| {
                                    let key = format!("{}", &new_parameter_name);
                                    let value = format!("{}", &new_parameter_value);
                                    add_parameter(key, value, cx.clone());
                                },
                                "+"
                            }
                        }
                    }
                }
            }
        }
    })
}

fn BodyOptions(cx: Scope) -> Element {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().read();

    let request_body = state.current.body.clone();
    let ctype = state.current.headers.get("content-type");

    cx.render(rsx!{
        div { class: "body",
            div { display: "grid", grid_template_columns: "45% 45%", justify_content: "space-evenly",
                select { 
                    class: "request-content-select",
                    onchange: move |evt| {
                        update_request_field("content-type", evt.value.clone(), cx.clone())
                    },
                    option { value: "none", "Select a Content-Type" }
                    option { value: "application/json", "application/json" }
                    option { value: "application/xml", "application/xml" }
                    option { value: "application/x-www-form-urlencoded", "application/x-www-form-urlencoded" }
                    option { value: "application/octet-stream", "application/octet-stream" }
                    option { value: "application/pdf", "application/pdf" }
                    option { value: "text/plain", "text/plain" }
                    option { value: "text/html", "text/html" }
                    option { value: "text/css", "text/css" }
                    option { value: "text/csv", "text/csv" }
                }
                button {
                    class: "beautify-button", 
                    "Beautify" 
                }
            }
            div { 
                class: "request-option-container",
                textarea { 
                    class: "request-body",
                    oninput: move |evt| {
                        update_request_field("body", evt.value.clone(), cx.clone())
                    },
                    "{request_body}" 
                }
            }
        }
    })
}

fn add_header(key: String, value: String, cx: Scope) {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().write();
    state.current.headers.insert(key, value);
}

fn remove_header(key: &str, cx: Scope) -> Option<String> {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().write();
    state.current.headers.remove(key)
}

fn HeaderOptions(cx: Scope) -> Element {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().read();

    let cloned_headers = state.current.headers.clone();
    
    let new_header_name = use_state(cx, || String::new());
    let new_header_value = use_state(cx, || String::new());

    cx.render(rsx!{
        div { class: "params",
            div { class: "request-option-container",
                border: ".25em inset black",
                div {
                    padding: ".25em",
                    background_color: "lime",
                    color: "black",
                    justify_content: "space-between",
                    display: "flex",
                    span {
                        "key"
                    }
                    span {
                        "value"
                    }
                    span {
                        ""
                    }
                }
                table { 
                    width: "100%", 
                    border_collapse: "collapse",
                    for (key , value) in cloned_headers {
                        tr {
                            td { class: "option-container", input { class: "option-input", value: "{key}" } }
                            td { class: "option-container", input { class: "option-input", value: "{value}" } }
                            td { class: "option-container",
                                button {
                                    class: "option-input",
                                    onclick: move |_| {
                                        let key_clone = key.clone();
                                        remove_header(&key_clone, cx.clone());
                                    },
                                    "x"
                                }
                            }
                        }
                    }
                    tr {
                        td { class: "option-container",
                            input {
                                class: "option-input",
                                placeholder: "Param Option 1",
                                oninput: move |evt| new_header_name.set(evt.value.clone()),
                                "{new_header_name}"
                            }
                        }
                        td { class: "option-container",
                            input {
                                class: "option-input",
                                placeholder: "Value 1",
                                oninput: move |evt| new_header_value.set(evt.value.clone()),
                                "{new_header_value}"
                            }
                        }
                        td { class: "option-container",
                            button {
                                class: "option-input",
                                onclick: move |_| {
                                    let key = format!("{}", &new_header_name);
                                    let value = format!("{}", &new_header_value);
                                    add_header(key, value, cx.clone());
                                    new_header_name.set(String::new());
                                    new_header_value.set(String::new());
                                },
                                "+"
                            }
                        }
                    }
                }
            }
        }
    })
}

#[derive(Props)]
struct ResponseScreen<'a> {
    rcontainer: &'a str,
}

fn ResponseDisect<'a>(cx: Scope<'a, ResponseScreen<'a>>) -> Element {
    cx.render(rsx!{
        div { 
            class: "response-container", 
            match cx.props.rcontainer {
                "Body" => {rsx!( ResponseBody {} )}
                "Headers" => {rsx!( ResponseHeaders {})}
                "Cookies" => {rsx!( ResponseHeaders {}) } 
                &_ => {rsx!(CaptureView{})}
            }
        }
    }) 
}



fn ResponseBody(cx: Scope) -> Element {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().read();

    if let Some(response) = &state.current.response {
        if response.body.is_empty() {
            cx.render(rsx!{
                div { 
                    class: "body",
                    div { class: "response-body-container",
                        textarea { 
                            class: "response-body old-skool", 
                            "No body received.." 
                        }
                    }
                }
            })
        } else {
            //let response_body = response.body.clone();
            cx.render(rsx!{
                div { class: "body",
                    div { class: "response-body-container",
                            textarea { 
                            class: "response-body old-skool", 
                            "{response.body}" 
                        }
                    }
                }
            })
        }
    } else {
        cx.render(rsx!{
            div { class: "body",
                div { class: "response-body-container",
                    textarea { 
                        class: "response-body", 
                        "No response received.." 
                    }
                }
            }
        })
    }
}

fn ResponseHeaders(cx: Scope) -> Element {
    let state = use_shared_state::<StateRequests>(cx).unwrap().read();
    //let cloned_headers = state.current.response.headers.clone();
    if let Some(response) = &state.current.response {
        if response.headers.is_empty() {
            cx.render(rsx!{
                div { class: "response-option-container",
            div {
                padding: ".25em",
                background_color: "black",
                color: "lime",
                justify_content: "space-around",
                display: "flex",
                span {
                    "key"
                }
                span {
                    "value"
                }
            }
        }
            })
        } else {
            let cloned_headers = response.headers.clone();
            cx.render(rsx!{
                div { class: "response-option-container",
            div {
                padding: ".25em",
                background_color: "black",
                color: "lime",
                justify_content: "space-around",
                display: "flex",
                span {
                    "key"
                }
                span {
                    "value"
                }
            }
            table { 
                width: "100%", 
                border_collapse: "collapse",
                for (key , value) in cloned_headers {
                    tr {
                        display: "flex",
                        td { class: "response-option", border_right: "none", "{key}" }
                        td { class: "response-option", "{value}" }
                    }
                }
            }
        }
            })
        }
    } else {
        cx.render(rsx!{
            div { class: "response-option-container",
            div {
                padding: ".25em",
                background_color: "black",
                color: "lime",
                justify_content: "space-around",
                display: "flex",
                span {
                    "key"
                }
                span {
                    "value"
                }
            }
        }
        })
    }
}

#[derive(Props)]
struct TrafficScreen<'a> {
    tcontainer: &'a str,
}

fn TrafficView<'a>(cx: Scope<'a, TrafficScreen<'a>>) -> Element {
 
    cx.render(rsx!{
        div { class: "traffic-overflow-container",
            match cx.props.tcontainer {
                "Captures" => {rsx!( CaptureView {}) } 
                "Saved" => {rsx!( SavedView {})}
                "History" => {rsx!( HistoryView {} )}
                &_ => {rsx!(CaptureView{})}
            }
        }
    })
}

#[derive(Deserialize, Serialize, Clone)]
struct ReceivedRequest {
    name: String,
    method: String,
    url: String,
    headers: HashMap<String, String>
}

#[derive(Deserialize, Serialize, Clone, Debug)]
struct CapturedRequest {
    name: String,
    method: String,
    url: String,
    headers: HashMap<String, String>
}

struct Capture {
    status_code: String,
    method: String,
    url: String,
}

// Captures
fn CaptureView(cx: Scope) -> Element {
    
    let start = Capture {
        status_code: "18:37".to_string(),
        method: "GET".to_string(),
        url: "example.com".to_string()
    };
    let mut caps = vec![start];

    let cname = "Captures";

    cx.render(rsx! {
        div {
            div { class: "traffic-header",
                span { 
                    class: "traffic-option-value",
                    "ID" 
                }
                span { 
                    class: "traffic-option-value",
                    "Method" 
                }
                span { 
                    class: "traffic-option-value",
                    "Status" 
                }
                span { 
                    class: "traffic-option-value",
                    "Domain" 
                }
            }
            // Include rows in the main block
            for (index, item) in caps.iter().enumerate() {
                div { 
                    class: "traffic-option",
                    onclick: move |evt| {
                        load_request(index, cx.clone())
                    },
                    span { 
                        class: "traffic-option-value",
                        "{index}" 
                    }
                    span { 
                        class: "traffic-option-value",
                        "{item.method}" 
                    }
                    span { 
                        class: "traffic-option-value",
                        "{item.status_code}" 
                    }
                    span { 
                        overflow_x: "clip",
                        class: "traffic-option-value",
                        "{item.url}" 
                    }
                }
            }
        }
    }
    )
}

fn add_saved(cx: Scope) {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().write();
    
    // Collect current values
    let save_name = state.current.name.clone();
    let save_description = state.current.description.clone();
    let save_method = state.current.method.clone();
    let save_url = state.current.url.clone();
    let save_headers = state.current.headers.clone();
    let save_parameters = state.current.parameters.clone();
    let save_body = state.current.body.clone();

    let new_data = CurrentRequest {
        name: save_name.to_string(),
        description: save_description.to_string(),
        method: save_method.to_string(),
        url: save_url.to_string(),
        parameters: save_parameters,
        body: save_body.to_string(),
        headers: save_headers,
        response: None
    };
    
    state.saved.push(new_data);
}

fn remove_saved(index: usize, cx: Scope){
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().write();
    if index < state.saved.len() {
        state.saved.remove(index);
    }
}

fn SavedView(cx: Scope) -> Element {
    let state = use_shared_state::<StateRequests>(cx).unwrap().read();

    let saved = state.saved.clone();

    cx.render(rsx! {
        div {
            div { class: "traffic-header",
                span { 
                    class: "traffic-option-value",
                    "ID" 
                }
                span { 
                    class: "traffic-option-value",
                    "Method" 
                }
                span { 
                    class: "traffic-option-value",
                    "Name" 
                }
                span { 
                    class: "traffic-option-value",
                    "Clear" 
                }
            }
            // Include rows in the main block
            for (index, item) in saved.iter().enumerate() {
                div { 
                    class: "traffic-option",
                    onclick: move |evt| {
                        load_request(index, cx.clone())
                    },
                    span { 
                        class: "traffic-option-value",
                        "{index}" 
                    }
                    span { 
                        class: "traffic-option-value",
                        "{item.method}" 
                    }
                    span { 
                        class: "traffic-option-value",
                        "{item.name}" 
                    }
                    span { 
                        overflow_x: "clip",
                        class: "traffic-option-value",
                        onclick: move | _| {
                            remove_saved(index, cx.clone())
                        },
                        "X" 
                    }
                }
            }
        }
    }
    )
}

fn load_request(index: usize, cx: Scope){
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().write();
    if let Some(success) = state.history.get(index).cloned() {
        state.current.url =  success.url.clone();

        if let Some(cloned_response) = success.response.as_ref().cloned() {
            if let Some(current_response) = &mut state.current.response {
                current_response.body = cloned_response.body;
            } else {
                println!("NO current response");
            }
        } else {
            println!("Cloned response has none!")
        }

    } else {
        println!("Out of bounds!");
    }
}

fn add_history(new_data: RequestHistory, cx: Scope) {
    let mut state = use_shared_state::<StateRequests>(cx).unwrap().write();

    let cap_request = use_future(cx, (), |_| async move {
        reqwest::get("http://localhost:8069/captured")
            .await
            .unwrap()
            .text()
            .await
    });
    
    match cap_request.value() {
        Some(Ok(response)) => {
            let jdata: Result<Vec<CapturedRequest>, serde_json::Error> = serde_json::from_str(response);
            if let Ok(captured_requests) = jdata {
                for req in &captured_requests {
                    let headers_map = req.headers.iter()
                    .map(|(key, value)| {
                        (key.to_string(), value.to_string())
                    })
                    .collect::<HashMap<_, _>>();
                    //let mut new_capture = RequestHistory {
                        //status_code: "69:420".to_string(),
                        //method: format!("{}", req.method),
                        //url: format!("{}", req.url),
                        //headers: headers_map,
                        //body: "examplebody".to_string(),
                    //};
                    //state.history.push(new_capture);
                }
            } else if let Err(e) = jdata {
                println!("Error occurred: {:?}", e);
            }
        },
        Some(Err(_)) => {println!("oops!")},
        None => {println!("Nope")},
    }
}

fn HistoryView(cx: Scope) -> Element {
    let state = use_shared_state::<StateRequests>(cx).unwrap().read();

    cx.render(rsx! {
        div {
            div { class: "traffic-header",
                span { 
                    class: "traffic-option-value",
                    "ID" 
                }
                span { 
                    class: "traffic-option-value",
                    "Method" 
                }
                span { 
                    class: "traffic-option-value",
                    "Status" 
                }
                span { 
                    class: "traffic-option-value",
                    "Domain" 
                }
            }
            // Include rows in the main block
            for (index, item) in state.history.iter().enumerate() {
                match &item.status {
                    RequestState::Loading => {
                        rsx!(
                            div { 
                                class: "traffic-option",
                                onclick: move |evt| {
                                    load_request(index, cx.clone())
                                },
                                span { 
                                    class: "traffic-option-value",
                                    "{item.id}" 
                                }
                                span { 
                                    class: "traffic-option-value",
                                    "{item.method}" 
                                }
                                span { 
                                    class: "traffic-option-value",
                                    "Loading..." 
                                }
                                span { 
                                    overflow_x: "clip",
                                    class: "traffic-option-value",
                                    "{item.url}" 
                                }
                            }
                        )
                    }
                    RequestState::Completed => {
                       rsx!(
                        div { 
                            class: "traffic-option",
                            onclick: move |evt| {
                                load_request(index, cx.clone())
                            },
                            span { 
                                class: "traffic-option-value",
                                "{item.id}" 
                            }
                            span { 
                                class: "traffic-option-value",
                                "{item.method}" 
                            }
                            span { 
                                class: "traffic-option-value",
                                match &item.response{
                                    Some(response) => {
                                        format!("{}", response.status_code) 
                                    },
                                    None => {
                                        "No response..".to_string()
                                    }
                                }
                            }
                            span { 
                                overflow_x: "clip",
                                class: "traffic-option-value",
                                "{item.url}" 
                            }
                        }
                       )
                    }
                }
            }
        }
    }
    )
}

#[derive(Props)]
struct ProjectScreen<'a> {
    container: &'a str,
}

fn ProjectScreen<'a>(cx: Scope<'a, ProjectScreen<'a>>) -> Element {
 
    cx.render(rsx!{
        div { class: "traffic-overflow-container",
            match cx.props.container {
                "Config" => {rsx!( ConfigView {})}
                "Sessions" => {rsx!( SessionView {}) } 
                "Traffic" => {rsx!(SessionView {})}
                &_ => {rsx!({})}
            }
        }
    })
}

fn ConfigView(cx: Scope) -> Element {
    let state = use_shared_state::<StateRequests>(cx).unwrap().read();
    
    let project_title = state.project.title.clone();
    let project_version = state.project.version.clone();
    
    cx.render(rsx! {
        div {
            div {
                class: "project-info-container",
                span {
                    class: "project-info-label",
                    "Project Title:"
                }
                input {
                    class: "project-info-input",
                    value: "{project_title}",
                    placeholder: "Enter project title",
                    oninput: move |evt| {
                        update_request_field("title", evt.value.clone(), cx.clone())
                    }
                }
            }
            div {
                class: "project-info-container",
                span {
                    class: "project-info-label",
                    "Project Version"
                }
                input {
                    class: "project-info-input",
                    value: "{project_version}",
                    placeholder: "Enter a project version",
                    oninput: move |evt| {
                        update_request_field("version", evt.value.clone(), cx.clone())
                    }
                }
            }
        }
    })
}

fn SessionView(cx: Scope) -> Element {
    let state = use_shared_state::<StateRequests>(cx).unwrap().read();

    cx.render(rsx! {
        div {
            div { class: "traffic-header",
                span { 
                    class: "traffic-option-value",
                    "ID" 
                }
                span { 
                    class: "traffic-option-value",
                    "Method" 
                }
                span { 
                    class: "traffic-option-value",
                    "Status" 
                }
                span { 
                    class: "traffic-option-value",
                    "Domain" 
                }
            }
        }
    }
    )
}