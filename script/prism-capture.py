import requests
from mitmproxy import http

def request(flow: http.HTTPFlow) -> None:
    # Only process HTTP requests (not WebSocket, etc.)
    if isinstance(flow.request, http.HTTPRequest):
        # Extract request details
        method = flow.request.method
        url = flow.request.url
        headers = {k: v for k, v in flow.request.headers.items()}

        # Construct the payload for the Actix web server
        data = {
            'url': url,
            'method': method,  # This can be removed if you don't need it in ReceivedRequest
        }
        
        # Make a request to the Actix web endpoint
        try:
            requests.post(
                "http://localhost:8069",
                json=data,
                headers={'Content-Type': 'application/json'},
                timeout=2  # seconds; you can adjust this
            )
        except requests.RequestException as e:
            flow.context.log.error(f"Error sending request to Actix server: {e}")
